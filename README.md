# renameFiles
Rename files with regex and GUI preview.
<p align="center"><img src="doc/example.png" alt="screenshot"/></p>

Invoke from command line with regex pattern and a PyQt GUI will list all matched files with checkboxes to select which to rename.  The new names are rendered alongside as a preview.

## Example
    renameFiles -r --ignorecase -f 'foo.*ar' -s NEW /tmp/demo

## License
Public Domain

